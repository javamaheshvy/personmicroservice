package com.example.person.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.person.model.Person;
import com.example.person.repository.PersonRepository;

@Service
public class PersonService {

	@Autowired
	private PersonRepository repository;

	public void save(final Person person) {
		repository.save(person);
	}

	public List<Person> getAll() {
		final List<Person> personList = new ArrayList<>();
		repository.findAll().forEach(person -> personList.add(person));
		return personList;
	}
}