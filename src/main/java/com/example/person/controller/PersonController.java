
package com.example.person.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.person.model.Person;
import com.example.person.service.PersonService;

@RestController
public class PersonController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PersonService personService;

	// Save person entity in the h2 database.
	@PostMapping(value = "/person/add")
	public int save(@RequestBody Person person) {
		log.info("save person details in the database.");
		personService.save(person);
		return person.getId();
	}

	// Get all persons from the h2 database.
	@GetMapping(value = "/person/list", produces = "application/json")
	public List<Person> getAll() {
		log.info("Retreive the person details from the database.");
		return personService.getAll();
	}
}
